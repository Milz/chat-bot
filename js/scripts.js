// BTN to TOP

    $(window).scroll(function() {
        if($(this).scrollTop() != 0) {
            $('#toTop').fadeIn();
        } else {
            $('#toTop').fadeOut();
        }
    });

    $('#toTop').click(function() {
        $('body,html').animate({scrollTop:0},800);
    });
    
    // chat

    const chat = document.querySelector('.chat');

    function createDivBot() {
        const divBot = document.createElement('div');
        divBot.className = "message message-bot";
        divBot.innerHTML = '<img class="preloader" src="images/800.gif" alt="">';
        return divBot;
    }

    function createInputMenu() {
        const inputMenu = document.createElement('div');
        inputMenu.className = "input-menu";
        return inputMenu;
    }

    function createDivCustomer() {
        const divCustomer = document.createElement('div');
        divCustomer.className = "message customer-message";
        return divCustomer;
    }

    function customerMessage(answer) {
        const divCustomer = createDivCustomer();
        divCustomer.innerHTML = `<p>${answer.innerHTML}</p>`;
        chat.appendChild(divCustomer);

        if(answer.dataset.answer == 'no') {

            if (answer.dataset.number == '0') {
                newBlock (answer)
            } else if (answer.dataset.number == '2') {
                newBlock (answer)
            } else if (answer.dataset.number == '2-1') {
                setTimeout(() => {
                    document.location.href = 'https://elegantsupercreative.xyz/tree-dress/'
                }, 500);
            } else if (answer.dataset.number == '3') {
                newBlock (answer)
            } else if (answer.dataset.number == '3-1') {
                setTimeout(() => {
                    document.location.href = 'https://elegantsupercreative.xyz/tree-dress/'
                }, 500);
            } else if (answer.dataset.number == '4') {
                newBlock (answer)
            } else if (answer.dataset.number == '4-1') {
                setTimeout(() => {
                    document.location.href = 'https://elegantsupercreative.xyz/tree-dress/'
                }, 500);
            } else if (answer.dataset.number == '5') {
                newBlock (answer)
            } else if (answer.dataset.number == '5-1') {
                setTimeout(() => {
                    document.location.href = 'https://elegantsupercreative.xyz/tree-dress/'
                }, 500);
            }  else if (answer.dataset.number == '6') {
                newBlock (answer)
            } else if (answer.dataset.number == '6-1') {
                setTimeout(() => {
                    document.location.href = 'https://elegantsupercreative.xyz/tree-dress/'
                }, 500);
            } else {
                setTimeout(() => {
                    document.location.href = 'https://homewow-office.xyz/top/?keyf=664347127327800&keyb=94nhazuxh1xq9loyn97j'
                }, 500);
            }
        } else if (answer.dataset.answer == 'yes') {
            newBlock (answer)
        } else {
            setTimeout(() => {
                document.location.href = 'https://homewow-office.xyz/top/?keyf=664347127327800&keyb=94nhazuxh1xq9loyn97j'
            }, 500);
        }
    }

    function botMessage(textDivBot, inputs, state) {
        if(state == 'record') {
            const answer = {
                text: textDivBot,
                inputs
            }
            localStorage.setItem('lastAnswer', JSON.stringify(answer))
        }
        const divBot = createDivBot();
        const inputMenu = createInputMenu();
        chat.appendChild(divBot);
        setTimeout(function() {
            divBot.innerHTML = textDivBot;
            window.scrollTo(0,document.body.scrollHeight);
        }, 1000);
        inputMenu.innerHTML = inputs;
        setTimeout(function() {
            chat.appendChild(inputMenu);
            window.scrollTo(0,document.body.scrollHeight);
        }, 2000);

        const input = inputMenu.querySelectorAll('.input-item');
        input.forEach(input => input.addEventListener('click', () => {
            setTimeout(() => {
                customerMessage(input);
                inputMenu.innerHTML = '';
            }, 300);
        }));
    }

    function newBlock (answer) {
        let textDivBot = '';
        let textInputs = '';

        if (answer.dataset.number == '0' && answer.dataset.answer == 'yes') {
            localStorage.removeItem('lastAnswer');
            init();

        } else {
                if (answer.dataset.number == '0' && answer.dataset.answer == 'no') {

                const local = JSON.parse(localStorage.getItem('lastAnswer'));
                [textDivBot, textInputs] =  [local.text, local.inputs];

            } else if (answer.dataset.number == '1') {

                textDivBot = '<p class="prog_time">Ваш размер в диапазоне 42-54?</p><img src="images/2.png" alt="">';
                textInputs = '<div class="input-item" data-answer="yes" data-number="2">Да, идем дальше </div><div class="input-item" data-answer="no" data-number="2">Нет</div>';

            } else if (answer.dataset.number == '2' && answer.dataset.answer == 'no') {

                textDivBot = '<p class="prog_time"> К сожалению, эта модель только в этой размерной сетке</p>';
                textInputs = '<div class="input-item" data-answer="yes" data-number="2-1">Посмотрю для подруги</div><div class="input-item" data-answer="no" data-number="2-1">Смотреть другие платья</div>';

            } else if ((answer.dataset.number == '2' && answer.dataset.answer == 'yes') || (answer.dataset.number == '2-1' && answer.dataset.answer == 'yes')) {

                textDivBot = '<p class="prog_time"> Тип Вашей фигуры «прямоугольник» или «песочные часы»? </p><img src="images/3.jpg" alt="">';
                textInputs = '<div class="input-item" data-answer="yes" data-number="3">Да, у меня такая. </div><div class="input-item" data-answer="no" data-number="3">Нет</div>';

            } else if ((answer.dataset.number == '3' && answer.dataset.answer == 'yes') || (answer.dataset.number == '3-1' && answer.dataset.answer == 'yes')) {

                textDivBot = '<p class="prog_time">Любите ли Вы карманы на платье?</p>';
                textInputs = '<div class="input-item" data-answer="yes" data-number="4">Да, это удобно</div><div class="input-item" data-answer="no" data-number="4">Нет, категорично</div>';

            } else if (answer.dataset.number == '3' && answer.dataset.answer == 'no') {

                textDivBot = '<p class="prog_time">Стилисты реккомендуют эту модель для таких типов фигур. Но выбор остается за Вами.</p>';
                textInputs = '<div class="input-item" data-answer="yes" data-number="3-1">Подходит, продолжить тест</div><div class="input-item" data-answer="no" data-number="3-1">Не подходит. Смотреть другие платья</div>';

            } else if ((answer.dataset.number == '4' && answer.dataset.answer == 'yes') || (answer.dataset.number == '4-1' && answer.dataset.answer == 'yes')) {

                textDivBot = '<p class="prog_time">Нравится ли Вам синий или зеленый цвета? </p><img src="images/4.gif" alt="">';
                textInputs = '<div class="input-item" data-answer="yes" data-number="5">Да, мне нравится</div><div class="input-item" data-answer="no" data-number="5">Терпеть не могу</div>';

            } else if (answer.dataset.number == '4' && answer.dataset.answer == 'no') {

                textDivBot = '<p class="prog_time">Даная модель с карманами </p>';
                textInputs = '<div class="input-item" data-answer="yes" data-number="4-1">Сделаю исключение, продолжить</div><div class="input-item" data-answer="no" data-number="4-1">Смотреть другие платья </div>';

            } else if ((answer.dataset.number == '5' && answer.dataset.answer == 'yes') || (answer.dataset.number == '5-1' && answer.dataset.answer == 'yes')) {

                textDivBot = '<p class="prog_time">Хотелось бы Вам, на лето, одевать приятную к телу и легкую ткань Soft?  </p>';
                textInputs = '<div class="input-item" data-answer="yes" data-number="6">Да! На лето - супер!</div><div class="input-item" data-answer="no" data-number="6">Нет, спасибо</div>';

            } else if (answer.dataset.number == '5' && answer.dataset.answer == 'no') {

                textDivBot = '<p class="prog_time">Модель есть в двух цветах: хаки и темно-синий</p>';
                textInputs = '<div class="input-item" data-answer="yes" data-number="5-1">Погорячилась, продолжить</div><div class="input-item" data-answer="no" data-number="5-1">Кошмар! Поищу что-то другое</div>';

            } else if ((answer.dataset.number == '6' && answer.dataset.answer == 'yes') || (answer.dataset.number == '6-1' && answer.dataset.answer == 'yes')) {

                textDivBot = '<p class="prog_time">Милая леди! Это платье идеально подходит Вам! Получите специальное ценовое предложение. </p><img src="images/5.jpg" alt="">';
                textInputs = '<div class="input-item" data-answer="yes" data-number="7">Получить предложение</div>';

            } else if (answer.dataset.number == '6' && answer.dataset.answer == 'no') {

                textDivBot = '<p class="prog_time">Данная модель из материала Soft.<br/> Состав: 50% вискоза, 50% полиэстер </p>';
                textInputs = '<div class="input-item" data-answer="yes" data-number="6-1">Хорошо, подходит</div><div class="input-item" data-answer="no" data-number="6-1">Я выберу что-то другое</div>';

            } else if (answer.dataset.number == '7' && answer.dataset.answer == 'yes') {
                setTimeout(() => {
                    document.location.href = 'https://homewow-office.xyz/top/'
                }, 500);
            }
            botMessage(textDivBot, textInputs, 'record');
        }
    }

    function init() {
        const local = localStorage.getItem('lastAnswer');
        if(local && JSON.parse(local).text.length > 0){
            const continueTextDivBot = '<p class="prog_time">Привет! Ты не первый раз здесь. Что ты хочешь?</p>';
            const continueInputs = '<div class="input-item" data-answer="yes" data-number="0">Начать сначала</div><div class="input-item" data-answer="no"  data-number="0">Продолжить</div>';
            botMessage(continueTextDivBot, continueInputs, 'norecord');
        } else {
            const startTextDivBot = '<p class="prog_time">Привет! 5 быстрых вопросов и ты поймешь, идеальное ли это платье для тебя</p><img src="images/1.png" alt="">';
            const startInputs = '<div class="input-item" data-answer="yes" data-number="1">Хорошо. Давай!</div><div class="input-item" data-answer="no"  data-number="1">Не надо. Я сама</div>';

            setTimeout(botMessage(startTextDivBot, startInputs, 'record'), 1000);
        }
    }

    init();

